import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row>
	        <Col>
	            <Card className="cardHighlight">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Record Your Expenses</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    	Every day. We mean it. Every. Day. If you don’t keep up with what you’re spending, you’ll be living in a fantasy land where wallets never go empty and bank accounts stay busting and full. That’d be great—but it isn’t the real world. In the real world, you have to keep up with what you spend.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col>
	            <Card className="cardHighlight">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Create a budget</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    	You won’t be able to track expenses without one. What’s a budget? It’s your monthly money plan—your expected income and expenses put in categories for the whole month.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col>
	            <Card className="cardHighlight">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Watch Those Amounts</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    	Tracking your expenses can help make sure you don’t overspend in any area. When you enter an expense, make sure you keep track of how much is left in that category
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>			
		</Row>
	)
}