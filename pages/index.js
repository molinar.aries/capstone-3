import Head from 'next/head'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights';

import { Container } from 'react-bootstrap';

export default function Home() {
  const data = {
    title: "Budget Expenses Tracker",
    content: "Record Income/Expenses. Monitor Income/Expenses. Save Money."
  }
  return (
    <React.Fragment>
      <Head>
        <title>Budget Expenses Tracker</title>
      </Head>
      <Banner data={data} />
      <Container>
        <Highlights />
      </Container>
    </React.Fragment>
  )
}