import { useState, useEffect } from 'react';
import { Table, Alert, Row, Col, Card, Button } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import Link from 'next/link'

export default function index(){
  const [categories, setCategories] = useState([])
  const [categoryType, setCategoryType] = useState('')

  useEffect(() => {
      let token = localStorage.getItem('token')

      fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-categories`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${token}`
          }
      })
      .then(res => res.json())
      .then(data => {
     
          if(data._id !== null){
              setCategories(data)
          }else{
              setCategories([])
          }
      })
  },[])
 
  return (
      <React.Fragment>
          <br/>
          <Link href="/addCategories">
              <a className="w-100 text-center d-flex justify-content-center text-dark" role="button">Add</a>
          </Link>
          {categories.length > 0
          ? <Table striped bordered hover>
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Type</th>
                  </tr>
              </thead>
              <tbody>
                  {categories.map(category =>{
                      return(
                          <tr key={category._id}>  
                              <td>{category.name}</td>
                              <td>{category.type}</td>                                    
                          </tr>
                      )
                  })
                  }
              </tbody>
          </Table>
          :   <Alert variant="warning">No categories added</Alert>
          }
      </React.Fragment>
  )
}